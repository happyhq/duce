import random
import base64

from paho.mqtt import client as mqtt_client


broker = '47.99.86.124'
port = 7001
topic = "com.weevent.test"
# generate client ID with pub prefix randomly
client_id = f'python-mqtt-{random.randint(0, 100)}'


def connect_mqtt() -> mqtt_client:
    def on_connect(client, userdata, flags, rc):
        if rc == 0:
            print("Connected to MQTT Broker!")
        else:
            print("Failed to connect, return code %d\n", rc)

    client = mqtt_client.Client(client_id)
    client.on_connect = on_connect
    client.connect(broker, port)
    return client


def subscribe(client: mqtt_client):
    def on_message(client, userdata, msg):
        resmsg=msg.payload.decode()

        print(f"Received `{resmsg}` from `{msg.topic}` topic")
        base64_message = base64.b64decode(resmsg).decode('ascii')
        print(base64_message)

    client.subscribe(topic,qos=1)
    client.on_message = on_message


def run():
    client = connect_mqtt()
    subscribe(client)
    client.loop_forever()


if __name__ == '__main__':
    run()